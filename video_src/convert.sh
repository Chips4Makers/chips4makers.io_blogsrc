#!/bin/sh

# Remove sound and rotate -90deg
ffmpeg -i 20180428_demo_XLR8_buspirate_orig.mp4 -an -vf rotate='-90*PI/180:ow=ih:oh=iw' 20180428_demo_XLR8_buspirate.mp4
