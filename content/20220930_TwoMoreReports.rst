Two More Reports on Scalable Analog blocks
==========================================

:date: 2022-09-30
:category: NLnet
:tags: AnaMS

The first results for the `NGI Zero PET <https://nlnet.nl/PET>`_ funded
`Analog/Mixed-Signal Library <https://nlnet.nl/project/AMSL/index.html>`__ project were
already presented in a `previous blog post <first-analogmixed-signal-project-results.html>`__.
Now the scalability reports on the rest of the analog blocks have been made available by
`LIP6 <https://www.lip6.fr>`__; one for a DAC and one for a PLL.

DAC
---

A comparison was made between the so-called R-2R and a capacitive DAC architecture.
Scaling implication have been investigated and a comparison of the two architectures were
made. You can find the details `in the report <static/20220930_DACReport.pdf>`__ authored by
`Marie-Minerve Louërat <https://www-soc.lip6.fr/users/marie-minerve-louerat/>`_.

PLL
---

Here the scalability implications of a classic PLL architecture with a phase-frequency
detector, a charge pump with loop filter and a voltage controlled oscillator (VCO) was
investigated. The details for this one can be found in `the corresponding report
<static/20220930_PLLReport.pdf>`__ authored by `Dimitri Galayko
<https://www-soc.lip6.fr/en/users/dimitri-galayko/>`__
