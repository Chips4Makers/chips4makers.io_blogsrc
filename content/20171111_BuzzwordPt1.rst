The Buzzword Article Part 1: Open Source
========================================

:date: 2017-11-12
:category: Retro-uC

We live in a world with mass production and consumption of goods. In order to stand out from the pack and to sell a lot of things the marketing of the goods is an important part of doing business in this setting. For this marketing often buzzwords are used to attract people.

We all like to think we are special and different than the rest. In reality we are mostly gregarious animals being part of herds in our work environment, sports club, church community, etc. Some of the really special people may earn their money as artists but most of them will be ignored or even become the avoided weirdo.

Being special we also think that the empty marketing buzzwords are not applicable to us but in reality the buzzwords do indicate trends and can be used as guidance to analyze use cases for a project. In the next few blog posts I will investigate some buzzwords and their applicability to the Chips4Makers project and it's Retro-uC pilot project. I will also dive into a few buzzwords not directly applicable to give more background on my reasoning behind the Chips4Makers project and my (twisted) thinking in general. The nice thing about humans being able to think is that we can also differ in opinion. I am happy to hear any feedback on my posts here by using the contact link in the left banner of this blog.

Open Source
-----------

Recently you can see open source mentioned in a lot of places. Sometimes it's not much more than a marketing term used to attract people but without much substance and used companies with a classic proprietary mind set. Some of my thoughts on the subject I used for a `presentation I did @ the last OrConf conference`_.

.. _`presentation I did @ the last OrConf conference`: https://www.youtube.com/watch?v=lQjC2NaaUAg&list=PLUg3wIOWD8ypZnjCc_M08APZ7NSuET4G1&index=24

For the Chips4Makers project I find open source an essential ingredient to make it possible. The purpose of the Chips4Makers project is to make real low volume custom chips possible. For mature technology nodes the production costs are not the main contributor anymore to the cost of a chip. The EDA_ software to design the chips and the licensing costs for IP_ sub-blocks are a major contributor. Open source is needed to reduce these costs.

.. _EDA: https://en.wikipedia.org/wiki/Electronic_design_automation

.. _IP: https://en.wikipedia.org/wiki/Semiconductor_intellectual_property_core

I don't envy the EDA_ software companies. For the state-of-the-art chip technology the process of designing the chips is becoming very complex. You not only have the exponential increase in number of devices on the chips themselves but also additional process complexities: increasing power density on the chips, double or multi-patterning, decreasing supply voltage or even dynamic voltage/frequency scaling, higher variation in performance between transistors on a chip and from chip to chip, etc. This means developing the tools to make chips for these nodes needs a lot of investment and this investment has to be earned back by licensing the software. Additionally if you ask a big stash of bucks to use your software they also expect reasonable support which needs trained people who can provide this. There is more than one EDA_ company so you also need sales people that try to convince their tool is better than their competitor's. This all increases the overhead and the total amount of money to be made by licensing of your software. For these state-to-the-art nodes the start-up costs for producing chips is several million of dollars so high (some would say professional) software license costs for the software are acceptable. For mature nodes with start-up cost going even down to only several thousands of dollars this is not the case. It is very difficult for the EDA_ companies to support the latter requirements; there is not enough money in that market to contribute to their bottom line. That's why I think that initiatives like the `FOSSi Foundation`_ or the `CAD and Open Hardware Devroom @ FOSDEM`_ are essential to get the open silicon movement from the ground.

.. _`FOSSi Foundation`: https://fossi-foundation.org

.. _`CAD and Open Hardware Devroom @ FOSDEM`: https://www.ohwr.org/projects/ohr-meta/wiki/FOSDEM2018

For the Chips4Makers and the Retro-uC project I will be using open source as much as possible and identify gaps in the tool chains and contribute improvements where time permits. I will be pragmatic in the use of proprietary tools. Contrary some other people I don't have ethical problems with the existence or the use of proprietary tools. I do think though a fully open source EDA_ tool chain is a long term requirement for a successful open silicon movement. Even if we would get support from some proprietary tool suite a change in strategy can cause problems in the future. An open source tool chain will always make it possible for people to take over and solve hiccups in the support or functionality.

Next to the software you also have the IP_ sub-blocks. For open silicon to succeed everyone reinventing the wheel all the time is not ideal. They should be able to stand on the shoulders of giants by reusing the HDL_ code of other projects. Here efforts like LibreCores_ and tools like FuseSoC_ are important parts of the puzzle. Problem is that the used HDL_ languages themselves seem to make it difficult to design generic re-usable code. Going into detail is going too far for this article but I do hope to find the time to go into more detail in a future post. For the Retro-uC pilot project I am using existing open source blocks and also the extra blocks that are developed will be made available for reuse in next projects. In that respect this pilot project is also a prove of the functionality of these blocks on a custom chip.

.. _HDL: https://en.wikipedia.org/wiki/Hardware_description_language

.. _LibreCores: https://www.librecores.org/

.. _FuseSoC: https://github.com/olofk/fusesoc

So for the Chips4Makers project to succeed I thus think open source is an essential part. In open source cooperation is import and I realize an open silicon movement can't be made successful by a single person. People with suggestions, propoals or just a (weird) idea to discuss should not hesitate to use the contact link provided on this blog site.
