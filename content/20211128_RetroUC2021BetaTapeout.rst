Retro-µC 2021 Test Tape-out
===========================

:date: 2021-11-28
:category: Retro-uC

.. image:: images/20211124_top_2dice_screenshot.png
    :width: 60%

I found some time to work on the Retro micro-controller again and
perform a test tape-out in TSMC 0.35um technology. I was also
contacted by Matt Venn for an interview. We agreed to also discuss
this tape-out during the interview. This relieved me from the (in
my eyes) boring task of writing an extensive blog post on it.
`The interview <https://www.youtube.com/watch?v=agXJeIpdU6I>`_ is
published on Matt Venn's `Zero To ASIC Course YouTube channel <https://www.youtube.com/c/ZeroToASIC>`_.

Just wanted to do a little additional expectation management. Due to
the current chip squeeze TSMC is less tolerant for canceled MPW
(multi-project wafer) reservations. In order to meet the deadline for
this run I did take several shortcuts, amongst others in verifying the
design and the P&R outcome. So I would not be surprised if this chip
isn't working. I did achieve the goal though of setting up the use of
Coriolis for place-and-route together with the standard cell library I am
developing.
