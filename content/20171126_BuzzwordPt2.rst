The Buzzword Article Part 2: Crowdfunding
=========================================

:date: 2017-11-26
:category: Retro-uC

In this second part of the article covering buzzwords I will be discussing crowdfunding which is also hot at the moment. The phenomenon is likely already over the 'Peak of Inflated Expectation' in the `Hype Cycle`_ and on the way to maturity. For the Chips4Makers project and open source silicon in general I think it is a major enabler.

.. _`Hype Cycle`: https://en.wikipedia.org/wiki/Hype_cycle

With classic investing one has to invest the money for development and first production run before going to market. Often external investor or investors are attracted to fund part of the money. But these investor(s) also expect to get return on their investment (ROI_) and will have thus their say on the direction of development. The owner of the idea and/or company is thus not for the fully 100% boss anymore and the investors will have influence; especially if original targets are not met. Additionally it often means a certain expectation on growth so just serving a niche market without pushing into other directions is most of the time not an option.

.. _ROI: https://en.wikipedia.org/wiki/Return_on_investment

With crowdfunding the money will be coming from much more people (e.g. the crowd). You have forms in a more classical setting where bonds or shares are sold to several people. Other type is the signle product financing crowdfunding of which Kickstarter_ is a known example. In this model for a specific product a target amount of money is put as a goal for needed investment and the product is produced when that target is reached with enough people donating. After production has completed the investors will then receive the product. This allows to test the market need for a product without the need to do a lot of pre-investing. Of course it is not without risks of the investors. Some unforeseen development costs or technical problems may deplete the invested money before the product is ready and have the investors still loose their money.

.. _Kickstarter: https://www.kickstarter.com/

One of the reasons I choose `Crowd Supply`_ for crowdfunding Retro-uC is that they actively help project owners resulting in a good track record in projects that succeeded in reaching their goal. Another reason is that they are focused on open source electronic products. I certainly want to avoid the ORSoC_ fiasco where they organized a crowdfunding campaign for an open source OpenRisc chip by themselves. They never reached their goal and the investors lost their money. By using `Crowd Supply`_ for the Retro-uC, investors know they will get the money back if the investment goal is not reached and allows me to test the market. I am currently spending quite some time on getting the financial picture clear before starting the campaign. This takes longer than expected and also is reason why the launch is delayed from the original November target. I do plan to go into more detail in one of the next blog posts on the adventure I am going trough.

.. _`Crowd Supply`: https://www.crowdsupply.com/
.. _ORSoC: http://www.orsoc.se/

After the Retro-uC project I also see an important role for crowdfunding for Chips4Makers to grow. Making custom chips has a high start-up cost, and to reduce that the chip manufacturers and semiconductor service companies provide `MPW services`_ where different projects are combined on one mask set and thus the start up costs are shared. These services can be used for open silicon projects but the restrictions on design size, timing, volume, NDAs, etc. may not be ideal for small volume production as they are optimized for prototype production. For Chips4Makers I do want to build on that by having several people launching crowdfunding campaigns for open silicon products. The final campaign is only successful if a number of projects reach the goal and these projects are then combined on one production run. This would optimize the cost and effort further, resulting in lower start-up cost and less volume needed per project than when projects would be run separately. All this is currently a dream and details can only be worked out after first successfully completing the Retro-uC pilot project.

.. _`MPW services`: https://en.wikipedia.org/wiki/Multi-project_wafer_service
