PDKMaster v0.2.0 and Demo Packages on Pypi
==========================================

:date: 2022-08-24
:category: NLnet
:tags: AnaMS

PDKMaster v0.2.0
----------------

In `previous post on first results for analog/mixed-signal
<first-analogmixed-signal-project-results.html>`_ I already mentioned I would update on the
status of PDKMaster development. This has taken me a few weeks more than planned but here
it is.

One of the targets in the `NGI Zero PET <https://nlnet.nl/PET>`_ funded
`Analog/Mixed-Signal Library <https://nlnet.nl/project/AMSL/index.html>`__ project is to
develop PDKMaster up to a state it can be used externally and is ready for external
contributions. The `v0.2.0 release <https://pypi.org/project/PDKMaster/0.2.0/>`_ of
`PDKMaster <https://gitlab.com/Chips4Makers/PDKMaster/>`__ is a step in this
direction. (`v0.2.1 release <https://pypi.org/project/PDKMaster/0.2.1/>`__
is a small bug fix release fixing a dependency issue)

First part was focused on documentation. Sphinx has been set up to extract autodoc strings
from the code and a first set of documentation is provided by adding some autodoc strings
to the PDKMaster code base. Currently the documentation is not hosted on a public web site
yet. This will be done once the code is deemed ready for external usage. For now you can
checkout the code, setting up the environment as said in the README.md file and the running
'``doit docs``' and then you will find the docs in the '``docs/html``' directory.

Second part was to work on QA. For this a first set of unit tests have been written and
`CI <https://gitlab.com/Chips4Makers/PDKMaster/-/pipelines>`__ has
been set up to run these unit tests. This is common development practice to avoid regressions
with new commits.

Demo Packages on Pypi
---------------------

For demo purposes also packages of the different Chips4Makers repos including the
PDKMaster based version of sky130 PDK have been released on `pypi  <https://pypi.org/>`__.
As said the code base is currently not considered to fit for external usage. All this is
released as-is and no support whatsoever can currently be provided. This is planned onwards
from release v0.9.0 of PDKMaster.

The list of related packages is:

* `c4m-flexcell v0.1.0 <https://pypi.org/project/c4m-flexcell/0.1.0/>`__
* `c4m-flexio v0.1.0 <https://pypi.org/project/c4m-flexio/0.1.0/>`__
* `c4m-flexmem v0.0.4 <https://pypi.org/project/c4m-flexmem/0.0.4/>`__
* `c4m-pdk-sky130 v0.0.2 <https://pypi.org/project/c4m-pdk-sky130/0.0.2>`__

One should be able to install everything with the command '``pip install c4m-pdk-sky130``'

PDKMaster v0.3.0
----------------

Although this release and announcement has taken some in the mean time work has already been
done on PDKMaster v0.3.0.

The target for that release:

* 100% code coverage for the unit tests
* autodoc strings for all classes and non-trivial functions and methods
* features needed for other tasks in the
  `Analog/Mixed-Signal Library <https://nlnet.nl/project/AMSL/index.html>`__