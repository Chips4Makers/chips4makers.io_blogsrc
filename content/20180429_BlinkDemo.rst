Blink Demo
==========

:date: 2018-04-29
:category: Retro-uC

Intro
-----

In order to show the progress I made on the Retro-uC a short video is presented with a demo with the Retro-uC running on an FPGA.

For the demo an `Alorium XLR8`_, a `Velleman VMA201 proto shield`_ and a `Dangerous Prototypes BusPirate V3.6`_ were used. On the proto shield LEDs and accompanying resistors have been soldered on the D2-D9 Arduino IOs.

.. _`Alorium XLR8`: http://www.aloriumtech.com/xlr8/
.. _`Velleman VMA201 proto shield`: https://www.velleman.eu/products/view/?id=435506
.. _`Dangerous Prototypes BusPirate V3.6`: `Dangerous Prototypes BusPirate V3.6`

The Retro-uC core used has the Z80 and the MOS6502 and a JTAG interface. In the video below a demo is giving by blinking the LEDs on the proto shield in four different ways:

#. The LEDs are blinked using the JTAG boundary scan method. This is a standardized way of accessing the IOs on a chip through the JTAG interface. This feature will also allow to easily test PCBs in a standard way that contains the final Retro-uC chip.
#. The LEDs are blinked by writing to the memory locations where the IOs are memory mapped. The JTAG interface in the Retro-uC has custom memory access commands and these can be used to write in the memory locations. On the Retro-uC the IOs are memory mapped and their state can be changed this way through the JTAG interface.
#. The LEDs are blinked by a Z80 program. The Retro-uC will have on-chip SRAM and on the XLR8 MAX10 block RAM is used. The JTAG memory access commands are now used to load the program in the on-chip RAM and enable the Z80 core. The Z80 is compiled from a C program using the `SDCC compiler`_.
#. The LEDs are blinked by a MOS6502 program. The same C program as before has now been compiled using the `cc65 compiler`_.

.. _`SDCC compiler`: http://sdcc.sourceforge.net/
.. _`cc65 compiler`: https://www.cc65.org/

Finally the Retro-uC is reset by pressing the Reset button.

The Video
---------

.. raw:: html

    <br /><video width="960" height="540" controls>
        <source src="videos/20180428_demo.mp4" type="video/mp4"/>
        <a href="videos/20180428_demo.mp4">Old browser video download</a>
    </video>

The Binaries and the Source
---------------------------

The source code for the demo is available on the `Retro-uC gitlab repo`_ and the `XLR8BlinkDemo_20180429 tag`_ corresponds with the state of the code as used for the demo. The file XLR8BlinkDemo_20180429.tgz_ attached to the tag contains the release files that allows to redo the demo on your own.

.. _`Retro-uC gitlab repo`: https://gitlab.com/Chips4Makers/Retro-uC
.. _`XLR8BlinkDemo_20180429 tag`: https://gitlab.com/Chips4Makers/Retro-uC/tags/XLR8BlinkDemo_20180429
.. _XLR8BlinkDemo_20180429.tgz: https://gitlab.com/Chips4Makers/Retro-uC/uploads/b7d727b88632e6828e696e474a4fc6c8/XLR8BlinkDemo_20180429.tgz

Some more detail on how the different files are generated (all files are referred to the top of the `Retro-uC gitlab repo`_ and commit of the tag):

* The bitstream for the MAX10 Altera FPGA on the XLR8 is generated with Quartus Prime Lite version 17.1.0. The project files are located in `boards/XLR8/quartus`_ with Retro-uC.pqf the top project file to be loaded in Quartus. The code uses a few git submodules (links also refer to commit as used for the demo):

  *  T80_: Z80 RTL code
  *  T65_: MOS6502 RTL code
  *  `C4M::JTAG`_: my own developed JTAG RTL code. Plan is to give more details on this code in one of the next blog posts.

  This is combined with Retro-uC specific code:

  * `rtl/vhdl`_: Generic Retro-uC RTL code, containing wrappers around the mentioned submodules and the top control module.
  * `boards/XLR8/rtl/vhdl`_: The XLR8 specific code, containing the top module that mainly maps the XLR8 specific IO pins to the generic one from Retro-uC.

* `boards/XLR8/demo`_: The demo files.

  * buspirate.cfg & playsvf.sh: Support files for using OpenOCD to access the JTAG interface
  * blink_boundaryscan.svf & blink_memmap.svf: hand written SVF files
  * In leds_c the C code is available together with the compile.sh scripts the generates the two other svf files using the two C comilers mentioned earlier.

.. _`Retro-uC gitlab repo`: https://gitlab.com/Chips4Makers/Retro-uC
.. _`boards/XLR8/quartus`: https://gitlab.com/Chips4Makers/Retro-uC/tree/08a6f51c3b1925018ceebd9ee99265d8e73c6110/boards/XLR8/quartus
.. _T80: https://gitlab.com/Chips4Makers/t80/tree/6c2b16cbd1c6398297cde6b0292471d41c9c7163
.. _T65: https://gitlab.com/Chips4Makers/t65/tree/b268bdafaa51ed33b7b7909df752e3bcd66f94d5
.. _`C4M::JTAG`: https://gitlab.com/Chips4Makers/c4m_jtag/tree/c412840d70267efc14843383e16933992ec1be7c
.. _`rtl/vhdl`: https://gitlab.com/Chips4Makers/Retro-uC/tree/08a6f51c3b1925018ceebd9ee99265d8e73c6110/rtl/vhdl
.. _`boards/XLR8/rtl/vhdl`: https://gitlab.com/Chips4Makers/Retro-uC/tree/08a6f51c3b1925018ceebd9ee99265d8e73c6110/boards/XLR8/rtl/vhdl
.. _`boards/XLR8/demo`: https://gitlab.com/Chips4Makers/Retro-uC/tree/08a6f51c3b1925018ceebd9ee99265d8e73c6110/boards/XLR8/demo

If you want to redo the demo from the release tarball the included README details how to program the XLR8 and run the tests. One warning though: when the Retro-uC core is programmed on the XLR8 it can't be accessed anymore through the Arduino IDE and needs to be accessed through the JTAG pins. So only do the demo if you feel comfortable with this.


Feedback
--------

I am interested in all feedback; be it questions & comments or ideas, tips, tricks etc. Feedback can be given on the log_ on the `Retro-uC Hackaday project page`_ or through an `email <mailto:staf@fibraservi.eu?subject=BlinkDemo>`_.

.. _`Retro-uC Hackaday project page`: https://hackaday.io/project/27091-chips4makers-pilot-retro-uc
.. _log: https://hackaday.io/project/27091-chips4makers-pilot-retro-uc/log/145585-retro-uc-blink-demo-on-xlr8
