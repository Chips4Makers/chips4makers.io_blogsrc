NLnet018TV Measurement Report
=============================

:date: 2021-05-19
:category: NLnet

Almost a year ago I reported on the `NLnet018TV test chip design <nlnet018tv-a-fully-automated-test-chip-design.html>`_ for `my NLnet project <https://nlnet.nl/project/Chips4Makers/index.html>`_.
I did have verification measurements of the chip available now for several weeks but because of the `Libre-SOC prototype tape-out <https://libre-soc.org/180nm_Oct2020/>`_ taking all of my time I did not find time and motivation to summarize the measurements for this blog up to now.

The test chip was manually wirebonded in 85-pin PGA package using `Europractice's standard packaging services <https://europractice-ic.com/packaging-integration/standard-packaging/>`_ through `imec <https://www.imec-int.com/>`_ as can be seen in the following picture:

.. image:: images/20210519_PackagedChip.jpg
    :width: 50%

For this PGA package a PCB adapter board was designed to make it easier to connect to the different pins. The KiCad design files of the PCB can be found `here <https://gitlab.com/Chips4Makers/snowwhite/-/tree/master/designs/NLNet018TV/testing/PGA85_Adapter>`__. For production of the PCBs I used `JLCPCB <https://jlcpcb.com/>`_. Below you find the picture of the PCB on the left and on the right the PCB with the chip and headers soldered on and pins connected for measurement:

.. image:: images/20210519_PGA85AdapterPCB.jpg
    :width: 38%
.. image:: images/20210519_PGA85AdapterSetup.jpg
    :width: 40%

I already reported on my adventures with the `AD5522 SMU <https://www.analog.com/en/products/ad5522.html>`_ in the `Poor Men's SMU <tag/pm-smu.html>`_ thread. I also used this SMU to meaure the NLnet018TV test chip.

The full report of the actual measurement can be found `here <static/20210519_NLNet018TV_MeasReport.html>`__; original notebook and saved data can be retrieved from the `snowwhite repository <https://gitlab.com/Chips4Makers/snowwhite/-/tree/master/designs/NLNet018TV/testing/AD5522/RaspberryPi>`_. The notebook is called `NLNet018TV_meas.ipynb` and the measured data `NLNet018TV.npz`. In the rest of the blog a summary of the measurements will be given. The main message is that no showstoppers were discovered for the Libre-SOC prototype tape-out; IO and SRAM cell are working fine.

SR Latches
----------

Functionality of SR latches has been succesfully verified and drive strength measured. More details are in the `measurement report <static/20210519_NLNet018TV_MeasReport.html>`_.

IO cells
--------

* The logic cell and IO themselves were succesfully powered by the designed IO cells
* For same designed drive strength the source current was relatively higher than the sink current (plot is below). For the prototpye the design is slightly adapted to bring them closer together.
* For the input the switching threshold is around 0.92V; this is still acceptable as it is higher than the 0.8V Vil of TTL and LVCMOS but ideally this would be closer to the 1.5V Vt of TTL and LVCMOS.

IO drive strength plot (pu=pull-up, pd=pull-down); left all curves, right zoom in on pull-up/pull-down functionality drive strength:

.. image:: images/20210519_IODriveStrength.png
    :width: 60%

The plots are clipped around 22µA due to measurement limits imposed by the `AD5522 SMU`_.

SRAM cell
---------

* The measured hold static noise margin (SNM) is around 600mV (plot below). This metric is a measuremnt for the SRAM robustness but detailed explanation goes too far for this blog. The measured value is very satisfactory as a value of 200-300mV would still be acceptable.
* Due to a design mistake read SNM could not be measured but write-trip point measurement still show robustness of the cell when word line is open and both bitlines are forced high.
* A write-trip point of around 0.80 V was measuremed (plot below); this is also a good value as there is enough margin to both ground and supply voltage.

SRAM plots, left hold SNM, right WTP.

.. image:: images/20210519_SRAM_SNMWTP.png
    :width: 60%

