The Buzzword Article Part 3: Makers
===================================

:date: 2017-12-31
:category: Retro-uC

In the third blog post in the discussion on buzzwords related to the Chips4Makers projects I will be talking about one that part of the name of the project.

The Hype
--------

The two biggest movements that can be seen as the forefront of the maker movement are the Arduino_ and the `Raspberry Pi`_. These two pieces have sold by the millions and have grown much bigger than expected. With both tools users can do things themselves and not buy an of the shelf part.

.. _Arduino: https://en.wikipedia.org/wiki/Arduino
.. _`Raspberry Pi`: https://en.wikipedia.org/wiki/Raspberry_Pi

When looking at the history of both projects it's interesting to see that both of them started with education in mind and not with profit as a first motif. Both of projects became commercial due to popularity and I would even use the word necessity. Arduino started as cheap teaching aid for non-technical students and the Raspberry as introductory tool to computing for kids. The history of Arduino is also filled with personal, academic and corporate infighting including some law suits. This shows that open source doesn't make a project immune for some human social imperfections.

Given the success of these two projects also interest was coming from the corporate world with projects like the `Intel Edison`_.  None of these initiatives have up to now been able to overtake the original projects given earlier. In general it's always quite difficult to overtake a successful first mover but it at least shows that open source projects can resist classic money driven corporate marketing push. Although difficult to derive from the facts, it's also my opinion that one of the reasons is that makers are more aware of what they actually want and thus less susceptible to classic marketing tricks. That's also a factor for me why I went with crowd-funding as explained in the `previous blog post`_.

.. _`Intel Edison`: https://en.wikipedia.org/wiki/Intel_Edison
.. _`previous blog post`: the-buzzword-article-part-2-crowdfunding.html

A Revival
---------

Actually the maker movement is not new but can be seen as a revival of the hacker movement from 80's/90's under a new name.

The original hacker movement was also driven by people who wanted to understand things and how things could be changed (hacked) but also by beliefs like that information wants to be free. Of course it was not always liked by the companies if hackers made public ways on how to phone for free or background information on the sleazy business and marketing tactics used. These companies then did lobbying and marketing to put the hacker movement in a bad light. True, this was also helped by the fact that the distinction was often unclear between the hacker movement and the cracker one; the latter who focused on (illegal) personal gain. Time has changed; the cracker movement has gone underground and producing things like malware and is highly driven by illegal derived gain. The thinkerers have now the maker movement and the information freedom, human rights, privacy and similar movements are now all a publicly represented by pressure groups.

I'm writing this on my way home from the `34C3 Conference`_; the 34th conference organized by Chaos Computer Club from Germany. This club and conference is thus already going from the 80's and have been was a big driving forces in the hacker movement.
It was my first time there although I was already familiar with the club from my young C64 and Amiga years. I was impressed by the organization of the conference where more than 15000 people met. No small feat I would say for a bunch of hackers and volunteers. Also the quality of the presentations was higher than I am used to from my 'professional' live. I suppose it's because all presenter are passionate about what they are doing and not their day to day job.

.. _`34C3 Conference`: https://events.ccc.de/congress/2017/wiki/index.php/Main_Page

The Difficult Road ?
--------------------

Given the nature of the makers/hackers trying to produce something they like is likely not the easiest road to take. But myself being a hacker/maker myself I would like to try it as it is something I am passionate about. I am also of the opinion open silicon would be liked by some of the makers. If I fail at least I have spent my time on something I am passionate about.

After having felt the maker/hacker vibe live at 34C3 I'm full of new energy to work further on continue the Retro-uC project and get through the slow progress in the discussions on the production.
