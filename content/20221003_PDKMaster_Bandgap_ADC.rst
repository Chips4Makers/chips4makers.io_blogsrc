PDKMaster circuits and layouts for bandgap and ADC
==================================================

:date: 2022-10-03
:category: NLnet
:tags: AnaMS

After a first pure report on analog blocks scalability, I am happy to be able to report on
the first PDKMaster based circuits and layouts for the `NGI Zero PET <https://nlnet.nl/PET>`_
funded
`Analog/Mixed-Signal Library <https://nlnet.nl/project/AMSL/index.html>`__ project in
this blog post. Reports on the scalability
`of a bandgap <static/20220621_BandgapReport.pdf>`__ and an
`ADC <static/20220621_SARADCReport.pdf>`__ circuits were already
discussed in a `previous blog post <first-analogmixed-signal-project-results.html>`__.
Now for these circuits a PDKMaster based circuit and layout generator have been implemented.

The bandgap implementation contains first version of code that does the sizing of the
transistors targeting minimum power consumption of the bandgap. The ADC implenentation
allows to quickly configure and generate a circuit and layout for an SAR ADC from 1 to 8
number of bits.

Voltage reference (e.g. bandgap)
--------------------------------

Based on the `bandgap report <static/20220621_BandgapReport.pdf>`__ a first implementation
has been made using the PDKMaster framework for a circuit and corresponding layout generation.
A notebook is used to generate these blocks; the output of the full run can be found
in the `bandgap notebook`_.

The ciruit and layout generation code can be found in
`c4m.pdk.sky130.bandgap <https://gitlab.com/Chips4Makers/c4m-pdk-sky130/-/blob/blog20221003/c4m/pdk/sky130/bandgap.py>`__
The classes in this module are ``Bandgap`` and ``BandgapCell``.

``Bandgap`` objects are only capable of circuit generation and simulation but no layout.
It is using continuous values for dimensions able to violate grid DRC rules etc. A first
version for the computation of an optimized version of a bandgap is implemented in the
`compute_minpower()`_ method of the class. The target is to minimize the power consumed by
the bandgap. This method thus shows how the simulation support in PDKMaster can be used
in optimization.

``BandgapCell`` objects on the other hand represent a circuit with corresponding layout
with dimensions of the structures that need to conform to the DRC rules. With the
`convert2cell()`_ method a ``Bandgap`` object can be converted to a ``BandgapCell`` one.
As this is the first version of the class this separation in two classes may in the future
be removed before a general bandgap structure is upstreamed in a more generalized library.

In the `bandgap notebook`_ one can see how this all is applied to three different
bandgap versions, one with 5V devices; one with low-Vt 1.8V devices and one with 
regular Vt 1.8V devices. First `compute_minpower()`_ is called and then converted to a
cell with layout using `convert2cell()`_.  
Normally the resulting reference voltage should be around 1.2V but one can see that for
the versions with 1.8V devices - even the low-Vt ones - this value is not reached.
This means that the Vt of these devices is too high relative to the supply voltage
to give enough operation margin for the current mirror to work in saturation.

Although the `compute_minpower()`_ seems to be able to compute a bandgap for the three
different varations of transistors it's also the first version that has room for
improvement. In good open source fashion this means that the code is open for
improvements or even for other stategies other than minimum power for computing the
bandgap transistor dimensions.

.. _`compute_minpower()`: https://gitlab.com/Chips4Makers/c4m-pdk-sky130/-/blob/blog20221003/c4m/pdk/sky130/bandgap.py#L577-L589
.. _`convert2cell()`: https://gitlab.com/Chips4Makers/c4m-pdk-sky130/-/blob/blog20221003/c4m/pdk/sky130/bandgap.py#L822-L836
.. _`bandgap notebook`: static/20221003_Bandgap.html

ADC
---

For the ADC it's `corresponding report <static/20220621_SARADCReport.pdf>`__ was used as
guidance. The choice was made to implement a single-ended ADC using the pre-amplifier
and the Lewis and Gray comparator as discussed in the document. The digital logic in
the sequencer has been changed slightly to better map to the cells available in the
`c4m-flexcell library`_; more detail is available as `comment in the code`_.
The simulation results in the `notebook run`_ shows that the adapted sequencer works
as expected.

In this case using nominal value transistors for most of the transistors gave
results that was fast enough for a first version.
So the focus in this excercise was more on easy configuration of the number of bits for
the ADC rather than on deriving an optimized version. From the `notebook run`_ one can
see the results of running the ADC generation for both an 8 bit and a 6 bit version and
it also results in a
`gds file with 2 ADCs <static/20221003_adclib_Sky130.gds>`__ in it. This shows the
flexibility in configuration of the number of bits for the ADC based on the flexibility of
the PDKMaster framework. In more classical approaches for analog design a 6 bit version and
a 8 bit version of an ADC would involve quite some more work in schematic capture and
layout generation.

As also said in the `notebook <static/20221003_ADC.html>`__ the use of PMOS switch to
capture the input voltage limits the input range of the ADC. This value will be further
investigated when an ADC will be put on a real tape-out.

For ADC the code has been done in `one class named ADC`_ in a file `ADC_support.py`_ local
to the notebook. `c4m-pdk-sky130 issue 4`_ has been filed to move the ADC code into c4m.pdk.sky130.

.. _`c4m-flexcell library`: https://gitlab.com/Chips4Makers/c4m-flexcell
.. _`comment in the code`: https://gitlab.com/Chips4Makers/c4m-pdk-sky130/-/blob/blog20221003/notebooks/ADC_support.py#L109-124
.. _`notebook run`: static/20221003_ADC.html
.. _`one class named ADC`: https://gitlab.com/Chips4Makers/c4m-pdk-sky130/-/blob/blog20221003/notebooks/ADC_support.py#L17
.. _`ADC_support.py`: https://gitlab.com/Chips4Makers/c4m-pdk-sky130/-/blob/blog20221003/notebooks/ADC_support.py
.. _`c4m-pdk-sky130 issue 4`: https://gitlab.com/Chips4Makers/c4m-pdk-sky130/-/issues/4

Simulation and layout support code
----------------------------------

For the simulations done for the bandgap some simulation support code has been provided in
`c4m.pdk.sky130._simulation <https://gitlab.com/Chips4Makers/c4m-pdk-sky130/-/blob/blog20221003/c4m/pdk/sky130/_simulation.py>`__.
This contains things like Ids vs Vgs simulation, result plotting support code etc.
This is thus more general code where at least part of it could be ripe for upstreaming;
`PDKMaster issue 26 <https://gitlab.com/Chips4Makers/PDKMaster/-/issues/26>`__
has been created to track this.

For the layout generation of both the bandgap and the ADC some common layout support code
has been used. It is available in
`c4m.pdk.sky130._layout <https://gitlab.com/Chips4Makers/c4m-pdk-sky130/-/blob/blog20221003/c4m/pdk/sky130/_layout.py>`__.
Currently a ``class Sky130Layout`` is defined which is higher level layout generation
code using ``class CircuitLayouter`` from PDKMaster internally.
`PDKMaster issue 25 <https://gitlab.com/Chips4Makers/PDKMaster/-/issues/25>`__ has been filed
for upstreaming the code.

Under construction
------------------

The code for these two blocks still lives in the dev branch with complicated
inter-project dependencies when writing this code. A branch named ``blog20221003``
has been made on the related repos that should be consistent. So if one wants to reproduce
the generation of the block one needs to checkout this branch for the ``PDKMaster``,
``c4m-flexcell``, ``c4m-flexio``, ``c4m-flexmem`` and the ``c4m-pdk-sly130`` repos. Don't
hesitate to contact me through one of the links on the left side if you want to try
this yourself.

Alternative is to wait until the layout generation code is upstreamed and the
circuit and layout generation live in the main branch(es) of the repo. After
completion of the
`PDKMaster v0.9.0 milestone <https://gitlab.com/Chips4Makers/PDKMaster/-/milestones/3>`__
- which is still planned for this month - things should become more fit for external use and external contributions.
