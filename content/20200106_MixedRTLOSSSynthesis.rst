Open source mixed RTL synthesis
===============================

:date: 2020-01-06
:category: Retro-uC

Test designs
------------

It has been some time that I posted here and it's one of my New Year Resolutions to be more active here. As reported in a `presentation @ ORConf 2019 <https://www.youtube.com/watch?v=xiBrZFaZ7hQ>`_ development of the Chips4Makers low volume ASIC manufacturing process is going on. Plan for this year is to do `a beta run <https://chips4makers.io/>`_ where other people can join.

In the ORConf presentation test chip tape-outs were presented. One of the test tape-outs contained a MOS6502 core and another a Z80 core. These cores are coming from FPGA implementations of old computers like a `ZX Spectrum <https://en.wikipedia.org/wiki/ZX_Spectrum>`_ or a `Commodore 64 <https://en.wikipedia.org/wiki/Commodore_64>`_. VHDL was used to write these cores. For the top cell and the JTAG interface nMigen is used which generates Yosys RTLIL or verilog as output. So for simulation and synthesis one need to mix different RTL languages.

Open source mixed RTL synthesis
-------------------------------

The Yosys open source synthesis tool chain has traditionally been focused on Verilog. So for make the Chips4Makers test chips a proprietary Verific plugin for the `Symbiotic EDA tool suite <https://www.symbioticeda.com/seda-suite>`_ was used to do the synthesis on the VHDL code. Luckily Tristan Gingold is working very hard on changing this story. If you look at the recent commits on the `ghdl repository <https://github.com/ghdl/ghdl/commits/master>`_ and the `ghdlsynth-beta repository <https://github.com/tgingold/ghdlsynth-beta/commits/master>`_ you can see a lot of VHDL synthesis related commits for GHDL and accompanying Yosys plugin.

The Yosys ghdlsynth plugin is now able to synthesize both the Z80 and MOS6502 VHDL core used for the test chips. This way I could redo the synthesis and the place-and-route for the test chip using only open source tools. In commit `77f33ae <https://gitlab.com/Chips4Makers/Retro-uC/commit/77f33ae0b2c8753df43b94203dcbb141d59dff51>`_ on the `Retro_uC repo <https://gitlab.com/Chips4Makers/Retro-uC>`_ the synthesis for the two test chips was switched to ghdlsynth-beta.

Repeating the work
------------------

If you want to repeat this you will need the latest version of quite some software and RTL code with some non-trivial compiling and python code installation. Best to `contact me <mailto:blog@chips4makers.io>`_ or head over to the `Chips4Makers Lobby <https://gitter.im/Chips4Makers/community>`_ on `gitter <https://gitter.im>`_.

After checking out the `Retro-uC repository <https://gitlab.com/Chips4Makers/Retro-uC>`_ you have to go into ``asic/C4M035/SnowWhiteIII_MOS6502`` or ``asic/C4M035/SnowWhiteIII_Z80`` directory and execute ``make gdsii`` there to do the generation of the RTL code with nMigen, synthesis with Yosys and place-and-route with QFlow.

You will need recent versions of several dependencies installed for this to work out:

* `yosys <https://github.com/YosysHQ/yosys>`_: synthesis software.
* `ghdl <https://github.com/ghdl/ghdl>`_: VHDL simulation and synthesis.
* `ghdlsynth-beta <https://github.com/tgingold/ghdlsynth-beta>`_: GHDL synthesis plugin for Yosys.
* `nMigen <https://github.com/m-labs/nmigen/>`_: python hardware generation framework.
* `t65 <https://gitlab.com/Chips4Makers/t65>`_: the MOS6502 VHDL core with a nMigen wrapper.
* `t80 <https://gitlab.com/Chips4Makers/t80>`_: the Z80 VHDL core with a nMigen wrapper.
* `c4m-jtag <https://gitlab.com/Chips4Makers/c4m-jtag>`_: reusable JTAG interface.
* `QFlow <http://opencircuitdesign.com/qflow/index.html>`_: RTL-2-GDSII flow; use version 1.4, with 1.3 you will need to use lower initial density settings.

Next step
---------

Currently I am still using the free but proprietary ModelSim for Altera FPGAs to do simulation. So the next step to take is to have a mixed RTL simulation flow using only open source tools.
