Poor Men's SMU, part 2
======================

:date: 2021-01-09
:category: NLnet
:tags: PM-SMU

This is a continuation of my adventure with the AD5522 `development board`_ (see `part 1`_ ). I did make progress on setting it up but as always it has taken much more time than wanted to debug the setup.

Schrödinger's cat
-----------------

One problem I had to solve was that the setup sometimes seemed to work flawlessly but other times not. Especially when I tried to debug the setup by looking at it though an oscilloscope all seemed to work but it did not seem to work when not looking at it.

After some more debugging I found out that the circuit did not work either when I used the oscilloscope probe in the 10X setting. This seems to indicate a problem with signal integrity that can be solved by adding load on the lines. I now added a 120Ω resistor on the SPI clock and chip select signals and my problems seem to be solved.

Raspberry Pi
------------

I decided to use a Raspberry Pi to access the SPI interface for the `AD5522`_ SMU and the `AD7685`_ ADC; giving the following setup:

.. image:: images/20210109_Setup.jpg
    :width: 80%

It's not pretty but ItWorks™. The `PT-65C`_ already shown in `part 1`_ is connected to the supply pins of the development board, the ground of the Raspberry Pi and the dev board are also connected together. Both chip select signals of SPI device 0 are used, the first for the `AD5522`_, the second for the `AD7685`_. Both the MISO from the `AD5522`_ and the `AD7685`_ are connected to the MISO pin of SPI device 0. This signal can be shared as it will only be driven by the chips when their chip select is activated.

I was still using Raspbian 9 based on the debian stretch release when doing the setup and I got it all working. As I am using python I did wanted to have a newer python as the python 3 version on stretch is 3.5 which like features like the f-strings etc. I decided to upgrade the version based on debian buster and now called Raspberry Pi OS. Unfortunately this also caused problems. The problem found is that the SPI driver on the latter version gives extra chip select signals.

In the following pictures the SPI clock is given in yellow and the chips select (active low) as purple. First the good one on older version of the OS:

.. image:: images/20210109_SPIStretch.png
    :width: 45%

Now the one on the latest version of the OS where one can see the second enabling of the chips select signal:

.. image:: images/20210109_SPIBuster.png
    :width: 45%

Unfortunately this extra chip select let the SMU perform a write to a register which makes it unusable. This has taken quite some time to debug as this problem was seen at the same time as the first problem and it took a long time to realize there are actually two problems. But in the end I seem to have ended with a working setup in the old Raspbian version.

Python notebook
---------------

As said earlier I am using python to drive the dev board through a SPI interface. Especially I am running a Jupyter notebook server on Raspberry Pi and accessing it from Firefox on my Linux amd64 desktop computer. I replicated now the IV curve done in `part 1`_ with the Analog Devices software in a Jupyter notebook (`IV Test notebook static version <static/IVTest.html>`_).

To be (hopefully) continued...

.. _`development board`: https://wiki.analog.com/resources/eval/ad5522_user_guide
.. _`part 1`: poor-mens-source-measurement-unit-smu.html
.. _`AD5522`: https://www.analog.com/en/products/ad5522.html
.. _`AD7685`: https://www.analog.com/en/products/ad7685.html
.. _`PT-65C`: https://www.meanwell.com/webapp/product/search.aspx?prod=PT-65
