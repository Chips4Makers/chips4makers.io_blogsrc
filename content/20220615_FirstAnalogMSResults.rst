First Analog/Mixed Signal Project Results
=========================================

:date: 2022-06-15
:category: NLnet
:tags: AnaMS

In a previous professional life I have been working on radiation hardened SRAM compiler design
amongst other things. There I used Cadence Virtuoso for analog circuit development. It is one
of the proprietary reference analog design platforms. As a hobby I was also doing open source
software development enjoying the big potential development acceleration that can be seen
through open source community based development. As an open source proponent I also longed
for a world where analog blocks would be developed with an open source mindset.

The main driver of the open source community is IMO not the availabillity of the source itself
but the process of having a whole community working on a gradual improvement of the open
source code base through smaller and bigger patches. Classically analog design is a lot of
times done as design to spec; e.g. the specification for a block is written down and then a
circuit is designed according to this specification. When specifications are changed
typically big part of the design and layout have to be redone. This methodology is not very
compatible with the gradual improvement flow hinted to above.

Recently the idea of (open source) scripted analog generators has seen renewed interest
(BAG: `paper <https://www2.eecs.berkeley.edu/Pubs/TechRpts/2019/EECS-2019-23.pdf>`_ & `github <https://github.com/ucb-art/BAG_framework>`_,
`OpenFASOC <https://fasoc.engin.umich.edu/>`_, `Oceane`_, ...)
due to the Sky130 open source PDK and the Google sponsored MPW runs. These developments are in
their early development phase and it is good that different approaches are investigated so a
good open source analog development methodology can grow out of these developments; including
cross-pollination between the projects.

Since last year I am working on a project funded by `NGI Zero PET <https://nlnet.nl/PET>`_ named
`Analog/Mixed-Signal Library <https://nlnet.nl/project/AMSL/index.html>`_.
As described on the project page, it consists on completing/extending `PDKMaster`_ to allow
scalable analog generators, use the framework on selected analog circuits and get them used
in a digital HDL-to-GDS ASIC flow. The focus of the project is on building the needed base
framework that will allow to use the open source gradual improvement workflow
also for analog circuits.

This blog post will focus on the progress on the analog circuits design. `PDKMaster`_ has
been discussed in a
`previous post <pdkmaster-v01-release-and-freepdk45-example.html>`_
but a lot of development has been done in the mean time. This will be discussed in a
follow-up post.

For the development of the `PDKMaster`_ based analog block generators four test structures were
selected:

* A voltage reference
* A PLL (phase-locked-loop)
* A lower precision, lower speed ADC (analog-to-digital converter)
* A lower precision, lower speed DAC (digital-to-analog converter)

One of the selection criteria for the circuits is to not have too complex circuits and where
applicable trade off performance for less analog circuit complexity. This is to
ensure development can be focused on the analog block generator rather than on
the needed analog design effort for the blocks itself. Once this project is finished the
source code of the blocks should then be the base code on which improvements can be made.

Analog circuits scaling reports
-------------------------------

The design of the analog block itself is done in cooperation with
`LIP6 <https://www.lip6.fr>`_ at the
`Sorbonne Université <https://www.sorbonne-universite.fr>`_. They are
doing a more classical design of the analog blocks but with a focus on having circuit
architectures that can be scaled to different technolgy nodes. They then make reports
on the blocks to be used by me (e.g. Staf Verhaegen) as base for the `PDKMaster`_
based analog block generators.

Three reports are now made available:

* Voltage reference report
* ADC report
* VCO report

Work is ongoing on completing the design reports on the full PLL circuit and a
DAC.

Voltage Reference
.................

The report on the voltage reference that is commonly known as a bandgap was made by
`Dimitri Galayko`_ and discusses the generic design of a relatively simple bandgap circuit.
The design has been tested in simulation for AMS 0.35µm and TSMC 0.18µm technology.
The report is provided as a `PDF file <static/20220621_BandgapReport.pdf>`__.

ADC
...

This report has been delivered by
`Marie-Minerve Louërat <https://www-soc.lip6.fr/users/marie-minerve-louerat/>`_ and
`Jacky Porte <https://www.lip6.fr/actualite/personnes-fiche.php?ident=P491>`_. It
discusses a SAR (successive approximation register) ADC (analog-to-digital converter). In
`the PDF <static/20220621_SARADCReport.pdf>`_, the design of an 8 bit SAR ADC is done using
the `Oceane`_ software is discussed together with scaling considerations.

VCO
...

VCO stands for voltage-controlled and is one the main subblocks in the PLL.
`Dimitri Galayko`_ has now made the report
on the VCO and node scalability; it is delivered again as a
`PDF file <static/20220621_VCOReport.pdf>`__.


First PDKMaster based Bandgap design
------------------------------------

Based on Dimitri's bandgap report a first version of a bandgap design using the PDKMaster
framework has been done in a `Sky130 python notebook <static/20220606_Bandgap3V3.html>`__.
In the notebook you can find the design of the circuit and some performance analysis.
This is currently a non-optimized bandgap design. Layout has been generated though and
has been submitted for `Sky130 MPW6 <https://platform.efabless.com/projects/1042>`__
(confirmation still pending).

.. image:: images/20220613_Bandgap3V3.png

Currently this is a preliminary version and will be further developed to allow the user to
for example trade-off between area, power consumption and accuracy. The design notebook
used for the bandgap design is also very preliminary as `PDKMaster`_ is still under heavy
development. As said above in a follow-up post the state of the `PDKMaster`_ development will
be discussed further.

.. _`Oceane`: https://www-soc.lip6.fr/equipe-cian/logiciels/oceane/
.. _`PDKMaster`: https://gitlab.com/Chips4Makers/PDKMaster
.. _`Dimitri Galayko`: https://www-soc.lip6.fr/en/users/dimitri-galayko/