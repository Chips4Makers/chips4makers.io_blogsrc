The Retro-uC is death - long live the Retro-uC !
================================================

:date: 2018-11-25
:category: Retro-uC

Retro-uC's death ?
------------------

Last month the `Retro-uC crowdfunding campaign <https://crowdsupply.com/chips4makers/retro-uc>`_ ended without reaching it's funding goal. I do regret that it did not reach it's goal but I did learn some things:

* The typical retro-computing guy is not very interested in Arduino type maker stuff. He wants to assemble and program a system with CPU, memory and peripherals. One chip with just GPIO outputs is not really getting him exciting.
* The prospect of an open silicon movement is not attractive enough for Arduino loving maker to pay some premium on an Arduino type device.

IMHO these factors meant the campaign stayed far away from the funding goal. The nice thing about crowdfunding is that you can find these things out without spending a lot of money.

One may wonder if this means the end of the Retro-uC and/or the Chips4Makers project. The answer is no, certainly not yet. Being a little wiser now I am even more motivated to work on help in getting the open silicon movement from the ground. I bought some new toys to play with and started looking at a good hardware 'programming' language.

New toys
--------

SRAM and I2C test chips
.......................

.. image:: images/Chips4Test.jpg
   :width: 45%
   :alt: SRAM and I2C EEPROM test chips

This was already mentioned in an update to the Retro-uC campaign but I ordered some SRAM and I2C EEPROM chips:

* `Fremont Micro FT24C32A 32 kB I2C EEPROM <https://fremontmicrousa.com/products-memories>`_
* `Microchip 24LC32A 32kB I2C EEPROM <https://www.microchip.com/wwwproducts/en/24LC32A>`_
* `IDT 71256SA 32 kB parallel SRAM <https://www.idt.com/products/memory-logic/srams/asynchronous-srams/71256sa-50v-32k-x-8-asynchronous-static-ram>`_
* `Alliance Memory AS6C62256 32 kB parallel SRAM <https://www.alliancememory.com/datasheets/as6c62256/>`_

This should be able to test an external SRAM interface and test the booting from external ROM for the Retro-uC. The external SRAM interface was an often asked for feature.

Arty S7 + Pmods
...............

.. image:: images/ArtyS7_Pmods.jpg
    :width: 45%
    :alt: Arty S7 FPGA board and PmodVGA plus PmodI2S2

I also ordered an Arty S7; this was the development board in one of the stretch goals. Also one of the reasons to go for this board is that is seems to have Migen support, see more below. The additional VGA and I2S Pmods allows to experiment with video and sound for Retro computing pleasure. They should also be usable on my `BlackIce <https://hackaday.io/project/12930-blackice-low-cost-open-hardware-fpga-dev-board>`_ board.

ZynqBerry
.........

.. image:: images/ZynqBerry_TE0726-03M.jpg
    :width: 65%
    :alt: ZynqBerry

Finally I ordered a `ZynqBerry <https://shop.trenz-electronic.de/de/TE0726-03M-ZynqBerry-Zynq-7010-in-Raspberry-Pi-Formfaktor>`_. This is a FPGA board with a `Xilinx Zynq SoC <https://www.xilinx.com/products/silicon-devices/soc/zynq-7000.html>`_ in a `Raspberry Pi 3 Model B <https://www.raspberrypi.org/products/raspberry-pi-3-model-b/>`_ form factor. One of the things I will investigate is how far this boards allows to combine some old-school Retro fun with modern features.

The quest for the ideal hardware 'programming' language
-------------------------------------------------------

As explained in a previous `blog post <retro-uc-first-time-right-yes-we-can.html>`_, I minimized the features for the Retro-uC. This was to not have feature creep generating long delays in the project and to minimize the risk of having a non-working part. But from comments it seems some more features are needed to make the Retro-uC more attractive.

I am not very fond of the current VHDL and (System)Verilog RTL languages. I am not going to go in-depth now but I do think that these languages are hampering productivity in developing working, debugged circuits. I do think they should be delegated to the same functionality as assembly language has for CPUs: basically only used by the higher level language compilers and in exceptional cases for low-level stuff. But even then maybe `FIRRTL <http://freechipsproject.github.io/firrtl/>`_ is more fit for this job.

Currently I am making myself familiar with `Migen <https://m-labs.hk/migen/>`_. For me this seems a perfect fit: it is python based and succesfully abstracts away the event based nature of the low level RTL languages. Other possible candidates were `Chisel <https://chisel.eecs.berkeley.edu/>`_ or `SpinalHDL <https://github.com/SpinalHDL/SpinalHDL>`_, but there I was put off by the reliance on the `Scala <https://www.scala-lang.org/>`_ language, `YMMV <https://dictionary.cambridge.org/us/dictionary/english/ymmv>`_. Most other HDL languages I looked at seemed not a good fit for me as they build on the same event driven basics which I do find are hampering the low-level RTL languages.
