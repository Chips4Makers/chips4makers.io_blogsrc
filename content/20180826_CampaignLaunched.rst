Retro-uC crowdfunding campaign launched on crowdsupply
======================================================

:date: 2018-08-26
:category: Retro-uC

I am delighted to announce that the Retro-uC crowdsupply campaign has now been launched. It has taken a little longer to straighten out the last hurdles in production and delivery but now the campaign is waiting for you. Together with the crowdsupply staff, the delay has also been used to streamline the pledge targets. Next to the hardware targets now also open silicon development support is included in some of the targets. This is to help to bootstrap the low-volume open silicon movement. For the details please head over to the `campaign page`_; use the Contact_ link for all your questions.

.. _`campaign page`: https://www.crowdsupply.com/chips4makers/retro-uc
.. _Contact: mailto:blog@chips4makers.io

Whether a retro geek or an open silicon enthusiast; now is the ideal time to show your support !
