Start of a new blog
===================

:date: 2017-10-22
:category: General

Inspired by the `LibreCores talk of Philipp Wagner`_ at `ORConf 2017`_ I now decided to start a blog
for the Chips4Makers project. Overall the `ORConf 2017`_ conference was inspiring and it was nice to
meet real hackers, makers and open source EDA enthousiasts.

When I was heading to the conference on a morning I saw this sign in a window:

.. image:: images/20170909_DreamBe.jpg
   :width: 400px
   :alt: Don't Dream It Be It!

.. _`LibreCores talk of Philipp Wagner`: https://www.youtube.com/watch?v=L4nO9Bmn-Ow&index=9&list=PLUg3wIOWD8ypZnjCc_M08APZ7NSuET4G1

.. _`ORConf 2017`: https://www.youtube.com/watch?v=0Hp5EMZSKrg&list=PLUg3wIOWD8ypZnjCc_M08APZ7NSuET4G1

I found it fit for the ORConf conference in general as there a lot of people who are contaminated
with the open source virus and wanting to bring what has started in the software world end of last century
and want to bring that also to the EDA and the silicon world. I did find it also fit for me. Since I
graduated mid nineties I have been full of ideas but typically never went to implementation. With
the Chips4Makers project I plan now to change that.

On this blog you can expect to find first-all more technical background on the Retro-uC pilot project
and other future projects. But you will also find some rumblings from me on open source, open silicon, EDA,
licensing and similar here. I do plan to also go further in some of the questions and comments I got on
my two ORConf presentations (1_, 2_).

.. _1: https://www.youtube.com/watch?v=4zNRTudEl_4&list=PLUg3wIOWD8ypZnjCc_M08APZ7NSuET4G1&index=13

.. _2: https://www.youtube.com/watch?v=lQjC2NaaUAg&list=PLUg3wIOWD8ypZnjCc_M08APZ7NSuET4G1&index=24

And for the people who have seen my ORConf presentation, it was a chock for me to discover I could not buy
an Amiga magazine anymore with the left over change when I was at the airport on my way home...
