ESD Generator: high voltage generator for ESD testing
=====================================================

:date: 2020-07-12
:category: NLnet

After tape-out of `NLNet018TV <nlnet018tv-a-fully-automated-test-chip-design.html>`_
I am now working on implementing the test procedures and gathering the equipment needed
for executing the tests.

For ESD testing high voltages with limited charges need to be generated. This high
voltage charge is then applied over two pins on the test chip to see if the chip
survives the event. This event mimics what happens when a charged person touches
a chip.

Standards exist for performing ESD tests together with expensive equipment that allows
to perform the test according to the standard. For our project we do want to perform tests
that also tests ESD performance but on a lower budget without claiming ESD standard
performance compliance.

A first version the schema of a circuit to generate high voltages up to 4kV has been designed
and is given in the next picture.

.. image:: images/20200712_ESDGeneratorv0.1.png
   :width: 90%

The circuit is a `Cockcroft-Walton generator <https://en.wikipedia.org/wiki/Cockcroft%E2%80%93Walton_generator>`_.
The schema is drawn in KiCad and available from `the gitlab repo <https://gitlab.com/Chips4Makers/snowwhite/-/tree/master/designs/NLNet018TV/testing/ESDGenerator>`_.

As I am not a very experienced PCB designer with even less experience with high voltages
I am sure there is much room for improvement. So don't hesitate to mail me your comments
or head over to the gitter page for a chat (links on the left).
