Retro-uC Campaign Product Options
=================================

:date: 2018-01-31
:category: Retro-uC

Finally I see a light at the end of the tunnel and is the launch of the Retro-uC campaign getting closer. In this post the different options for the campaign will be presented in their current state. Changes are still possible until the launch of the campaign. All feedback is highly appreciated, so head over to the `Hackaday.io project site for the Retro-uC`_ if you want to comment.

.. _`Hackaday.io project site for the Retro-uC`: https://hackaday.io/project/27091-chips4makers-pilot-retro-uc/log/86360-retro-uc-crowdfunding-campaing-product-options

Four options are currently foreseen:
 * The chip itself
 * The chip mounted on a board that is breadboardable
 * The chip mounted on a prototyping board
 * The chip integrated in a stand-alone board with Arduino compatible IO layout

All three PCB design are done in KiCad and available in the KiCad directory in the `Retro-uC git repository`_.

.. _`Retro-uC git repository`: https://gitlab.com/Chips4Makers/Retro-uC

The Chip
--------

The chip itself will contain the three retro cores: Z80, MOS6502 and Motorola 68000 compatible CPUs with some on-chip memory and lot's of IO. The size of the on-chip RAM is not fixed yet but is going to be a few KB.
For the package currently a 100-pin QFN package is assumed but this may also still change.

Originally two versions of the chip were investigated one with low number and one with a high number of IOs. But after further investigation it was seen that due to cost of sawing the smaller chips the cost difference would be small. With only one chip, the setup cost for packaging has to be paid only once and the logistics are also easier. Also the on-chip RAM available for the low IO chip would have been quite limited. Thus it was decided to go for only the big IO chip.

An overview of the pins:
 * VIO, VCORE, GND, GNDIO: The power pins, 4 of each are provided. The IO voltage will be 5V and the voltage for the logic on the chip will be 3.3V.
 * TCK, TMS, TDI, TDO, TRST_N: JTAG interface that is used for testing the chip but also used for loading programs in the on-chip RAM.
 * ENZ80, EN6502, ENM68K: Pins that can select which core is running. Currently only one core is planned to be active at the same time so there will be priorioty in between the chips. During further development it may be decided to allow more than one active. But only if there is minimal risk of having a non-functioning chip and if the boot sequences of the cores can be made compatible.
 * I2CBOOT: When this pins is high the on-chip RAM will be loaded from an external I2C Flash chip. It it is low the program will need to be loaded through the JTAG interface.
 * CLK, RESET_N: The clock and reset of the chip.
 * PA1-PA10, PB1-PB21, PC1-PC21, PD1-PD21: 73 General purpose 5V IOs

Breadboardable PCB
------------------

This is a PCB with the chip mounted that can be used on a breadboard. The chip can be used using only the two outer rows but if more IO capability is wanted this can be done through the inner via arrays.
There are four DIP switches on this board with which one can determine the state of the ENZ80, EN6502, ENM68K and the I2CBOOT pins. The board should then be functional when just providing the two supply voltages, a clock and an external I2C Flash chip that stores the program.

.. image:: images/20180131_Retro_uC-Breadboard-Front.png
   :width: 2.54cm
   :alt: Breadboardable PCB Front


.. image:: images/20180131_Retro_uC-Breadboard-Back.png
   :width: 2.54cm
   :alt: Breadboardable PCB Back

Prototyping PCB
---------------

This is a PCB with the chip mounted with prototyping areas inspired by the `Perf+ 2 prototyping PCB boards`_. The prototyping areas have horizontal copper lines on the front and vertical ones on the back. This allows to do routing by soldering and without the need of jumper wires. In between the prototyping area solder bridges are foreseen that allows to continue the horizontal and vertical traces. If the solder bridges are left open, DIL components can be mounted on the board without the need for cutting PCB traces.

.. _`Perf+ 2 prototyping PCB boards`: https://www.crowdsupply.com/ben-wang/perf-2

.. image:: images/20180131_Retro_uC-ProtoPlus-Front.png
   :width: 18.8cm
   :alt: Prototyping PCB Front


.. image:: images/20180131_Retro_uC-ProtoPlus-Back.png
   :width: 18.8cm
   :alt: Prototyping PCB Back

Retrino
-------

This is a board with IO layout compatible with the Arduino MEGA. Currently only digital 5V IO is planned to be supported, no analog features are present. The use case for the board is to be able to use Arduino (MEGA) shields from assembly programs using the supported retro instruction sets. There are currently no plans to port the Arduino IDE as then one could better use a standard Arduino board.

Next to the Retro-uC on the board is an stm32f0 ARM Cortex-M0 microcontroller from ST. It will allow to program the board through a USB connection with a computer and store the program in the on chip non-volatile memory. Once programmed the board can be used stand-alone by providing it 5V on the micro-USB. The ARM microcontroller with then boot the Retro-uC from it's on-chip non-volatile memory. Additionally a voltage-regulator is on board that will generate the 3.3V out of the 5V provided on the USB port.


.. image:: images/20180131_Retro_uC-Retrino-Front.png
   :height: 5.5cm
   :alt: Retrino Front


.. image:: images/20180131_Retro_uC-Retrino-Back.png
   :height: 5.55cm
   :alt: Retrino Back
