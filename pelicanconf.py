#!/usr/bin/env python
# -*- coding: utf-8 -*- #

AUTHOR = 'Fatsie'
SITENAME = 'Chips4Makers.io'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'en'

OUTPUT_PATH = '../chips4makers.io/blog'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('PDKMaster & co. docs', 'https://chips4makers.io/documentation'),
         ('Chips4Makers @ Matrix', 'https://matrix.to/#/#Chips4Makers_community:gitter.im'),
        )

# Social widget
SOCIAL = (('envelope', 'mailto:blog@chips4makers.io'),
          ('gitlab', 'https://gitlab.com/Chips4Makers'),
          ('twitter', 'https://twitter.com/Chips4Makers'),
          ('rss', '/blog/feeds/all.atom.xml'),
         )

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True

# Theme
THEME = "/home/verhaegs/software/no-stow/pelican-themes/Flex"
SITETITLE = "Chips4Makers"
SITESUBTITLE = "Chips want to be free"
SITELOGO = 'images/C4M-logo-300ppi.png'
MAIN_MENU = True
STATIC_PATHS = ['images', 'videos', 'static']
EXTRA_PATH_METADATA = {
    'extra/custom.css': {'path': 'static/custom.css'},
}
CUSTOM_CSS = 'static/custom.css'
READERS = {'html': None}

#JINJA_ENVIRONMENT = {'extensions': ['jinja2.ext.i18n']}
#PLUGIN_PATHS = ['/home/verhaegs/software/no-stow/pelican-plugins']
